//
//  HP+View.swift
//  TextileTrade
//
//  Created by mac on 25/08/17.
//  Copyright © 2017 mac. All rights reserved.
//

import UIKit

extension UIView{
    /// A property that accesses the backing layer's masksToBounds.
    @IBInspectable
    open var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set(value) {
            layer.masksToBounds = value
        }
    }
    
    /// A property that accesses the backing layer's opacity.
    @IBInspectable
    open var opacity: Float {
        get {
            return layer.opacity
        }
        set(value) {
            layer.opacity = value
        }
    }
    
    /// A property that accesses the backing layer's anchorPoint.
    @IBInspectable
    open var anchorPoint: CGPoint {
        get {
            return layer.anchorPoint
        }
        set(value) {
            layer.anchorPoint = value
        }
    }
    
    /// A property that accesses the frame.origin.x property.
//    @IBInspectable
//    open var x: CGFloat {
//        get {
//            return layer.x
//        }
//        set(value) {
//            layer.x = value
//        }
//    }
    
    /// A property that accesses the frame.origin.y property.
//    @IBInspectable
//    open var y: CGFloat {
//        get {
//            return layer.y
//        }
//        set(value) {
//            layer.y = value
//        }
//    }
    
    /// A property that accesses the frame.size.width property.
//    @IBInspectable
//    open var width: CGFloat {
//        get {
//            return layer.width
//        }
//        set(value) {
//            layer.width = value
//        }
//    }
//    
//    /// A property that accesses the frame.size.height property.
//    @IBInspectable
//    open var height: CGFloat {
//        get {
//            return layer.height
//        }
//        set(value) {
//            layer.height = value
//        }
//    }
    
    /// HeightPreset value.
//    open var heightPreset: HeightPreset {
//        get {
//            return layer.heightPreset
//        }
//        set(value) {
//            layer.heightPreset = value
//        }
//    }
    
    /**
     A property that manages the overall shape for the object. If either the
     width or height property is set, the other will be automatically adjusted
     to maintain the shape of the object.
     */
//    open var shapePreset: ShapePreset {
//        get {
//            return layer.shapePreset
//        }
//        set(value) {
//            layer.shapePreset = value
//        }
//    }
    
    /// A preset value for Depth.
//    open var depthPreset: DepthPreset {
//        get {
//            return layer.depthPreset
//        }
//        set(value) {
//            layer.depthPreset = value
//        }
//    }
    
    /// Depth reference.
//    open var depth: Depth {
//        get {
//            return layer.depth
//        }
//        set(value) {
//            layer.depth = value
//        }
//    }
    
    /// A property that accesses the backing layer's shadow
    @IBInspectable
    open var shadowColor: UIColor? {
        get {
            guard let v = layer.shadowColor else {
                return nil
            }
            
            return UIColor(cgColor: v)
        }
        set(value) {
            layer.shadowColor = value?.cgColor
        }
    }
    
    /// A property that accesses the backing layer's shadowOffset.
    @IBInspectable
    open var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set(value) {
            layer.shadowOffset = value
        }
    }
    
    /// A property that accesses the backing layer's shadowOpacity.
    @IBInspectable
    open var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set(value) {
            layer.shadowOpacity = value
        }
    }
    
    /// A property that accesses the backing layer's shadowRadius.
    @IBInspectable
    open var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set(value) {
            layer.shadowRadius = value
        }
    }
    
    /// A property that accesses the backing layer's shadowPath.
    @IBInspectable
    open var shadowPath: CGPath? {
        get {
            return layer.shadowPath
        }
        set(value) {
            layer.shadowPath = value
        }
    }
    
//    /// Enables automatic shadowPath sizing.
//    @IBInspectable
//    open var isShadowPathAutoSizing: Bool {
//        get {
//            return layer.isShadowPathAutoSizing
//        }
//        set(value) {
//            layer.isShadowPathAutoSizing = value
//        }
//    }
//    
//    /// A property that sets the cornerRadius of the backing layer.
//    open var cornerRadiusPreset: CornerRadiusPreset {
//        get {
//            return layer.cornerRadiusPreset
//        }
//        set(value) {
//            layer.cornerRadiusPreset = value
//        }
//    }
    
    /// A property that accesses the layer.cornerRadius.
    @IBInspectable
    open var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set(value) {
            layer.cornerRadius = value
        }
    }
    
    /// A preset property to set the borderWidth.
//    open var borderWidthPreset: BorderWidthPreset {
//        get {
//            return layer.borderWidthPreset
//        }
//        set(value) {
//            layer.borderWidthPreset = value
//        }
//    }
//    
    /// A property that accesses the layer.borderWith.
    @IBInspectable
    open var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set(value) {
            layer.borderWidth = value
        }
    }
    
    /// A property that accesses the layer.borderColor property.
    @IBInspectable
    open var borderColor: UIColor? {
        get {
            guard let v = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: v)
        }
        set(value) {
            layer.borderColor = value?.cgColor
        }
    }
    
    /// A property that accesses the layer.position property.
    @IBInspectable
    open var position: CGPoint {
        get {
            return layer.position
        }
        set(value) {
            layer.position = value
        }
    }
    
    
    
    /// A property that accesses the layer.zPosition property.
//    @IBInspectable
//    open var zPosition: CGFloat {
//        get {
//            return layer.zPosition
//        }
//        set(value) {
//            layer.zPosition = value
//        }
//    }
//    
//    /// Manages the layout for the shape of the view instance.
//    open func layoutShape() {
//        layer.layoutShape()
//    }
//    
//    /// Sets the shadow path.
//    open func layoutShadowPath() {
//        layer.layoutShadowPath()
//    }
}

extension UIViewController {
    
    //Previous code
    
    var activityIndicatorTag: Int { return 999999 }
    
    
    func startActivityIndicator(
        style: UIActivityIndicatorViewStyle = .whiteLarge,
        location: CGPoint? = nil) {
        
        //Set the position - defaults to `center` if no`location`
        
        //argument is provided
        
        let loc = location ?? self.view.center
        
        //Ensure the UI is updated from the main thread
        
        //in case this method is called from a closure
        
        DispatchQueue.main.async(execute: {
            
            //Create the activity indicator
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
            //Add the tag so we can find the view in order to remove it later
            
            activityIndicator.tag = self.activityIndicatorTag
            activityIndicator.color = UIColor.black //AppColor.appHeaderColor
            //Set the location
            
            activityIndicator.center = loc
            activityIndicator.hidesWhenStopped = true
            //Start animating and add the view
            
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
            self.view.isUserInteractionEnabled = false
        })
    }
    
    func stopActivityIndicator() {
        
        //Again, we need to ensure the UI is updated from the main thread!
        
        DispatchQueue.main.async(execute: {
            //Here we find the `UIActivityIndicatorView` and remove it from the view
            
            if let activityIndicator = self.view.subviews.filter(
                { $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                self.view.isUserInteractionEnabled = true
            }
        })
    }
}

extension UIImageView{
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
}

extension UIButton {
//    func setImageOnleftView(img : String ,leftPading : Bool, isBottomBorder : Bool ) {
//
//        if !img.isEmpty && leftPading {
//
//            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 50, height: 35) )
//            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 18, y: 10, width: 15, height: 15))
//            imgVw.image = UIImage(named: img)
//            vwForImg.addSubview(imgVw)
//            self.leftView = vwForImg
//            self.leftViewMode = UITextFieldViewMode.always
//
//        } else if !img.isEmpty {
//            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 25, height: 35) )
//            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 15, height: 15))
//            imgVw.image = UIImage(named: img)
//            vwForImg.addSubview(imgVw)
//            self.leftView = vwForImg
//            self.leftViewMode = UITextFieldViewMode.always
//
//        }  else if leftPading {
//            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 10, height: 35) )
//            self.leftView = vwForImg
//            self.leftViewMode = UITextFieldViewMode.always
//        }
//
//        if isBottomBorder {
//
//            let bottomLayerEmail = CALayer()
//            bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
//            bottomLayerEmail.backgroundColor = UIColor.black.cgColor
//            self.layer.addSublayer(bottomLayerEmail)
//            self.clipsToBounds = true
//        }
//    }
    

//    func underline() {
//        guard let text = self.titleLabel?.text else { return }
//
//        let attributedString = NSMutableAttributedString(string: text)
//        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: text.count))
//
//        self.setAttributedTitle(attributedString, for: .normal)
//    }
    
}
