//
//  CommunicationManager.swift
//  CoreDataStack
//
//  Created by Bipin Kumar on 17/08/16.
//  Copyright © 2016 Prabhat Kasera. All rights reserved.
//

import UIKit
import MobileCoreServices

class CommunicationManager: NSObject {
 
    let notificationName = Notification.Name("login")
    let defaults = UserDefaults.standard
    
    //TODO: App Transport Layer Security
    //TODO: Check all the alerts
    
//    public enum WebServiceType :Int {
//
//        case Login
//        case ForgotPassword
//        case SignUp
//        case VerifyOTP
//        case ResendOTP
//        case ResetPassword
//        case SuburbList
//        case GetAllServices
//        case GetFeaturedList
//        case Testimonial
//        case GetStylist
//        case UpdateProfilePic
//        case UpdateProfilePart
//        case Favourite
//        case StylistServices
//        case GetAppointmentsPast
//        case GetAppointmentsUpcoming
//        case ReviewList
//        case NotificationSetting
//        case StylistTimeSlots
//        case BookAppointment
//        case GetTimeslots
//        case ApplyPromocode
//        case CalculateTravelCharge
//        case EditAppointment
//        case CancelAppointment
//        case Review
//        case StylistTravelzone
//        case StylistProfile
//
//        func getRequestURLByType() -> String {
//
//            switch self {
//
//            case .Login:                        return "\(APPURL.BaseURLSeeker)login"
//            case .ForgotPassword:               return "\(APPURL.Domain)forgotPassword"
//            case .SignUp:                       return "\(APPURL.BaseURLSeeker)register"
//            case .VerifyOTP:                    return "\(APPURL.Domain)verifyotp"
//            case .ResendOTP:                    return "\(APPURL.Domain)resendotp"
//            case .ResetPassword:                return "\(APPURL.Domain)resetPassword"
//            case .SuburbList:                   return "\(APPURL.Domain)suburbs"
//            case .Testimonial:                  return "\(APPURL.Domain)testimonial"
//            case .GetAllServices:               return "\(APPURL.BaseURLSeeker)getAllServices"
//            case .GetFeaturedList:              return "\(APPURL.BaseURLSeeker)getFeaturedStylist"
//            case .GetStylist:                   return "\(APPURL.BaseURLSeeker)getStylist"
//            case .UpdateProfilePic:             return "\(APPURL.BaseURLSeeker)updateProfilePic"
//            case .UpdateProfilePart:            return "\(APPURL.BaseURLSeeker)updateProfilePart"
//            case .Favourite:                    return "\(APPURL.BaseURLSeeker)favourite"
//            case .StylistServices:              return "\(APPURL.BaseURLSeeker)stylistServices"
//            case .GetAppointmentsPast:          return "\(APPURL.BaseURLSeeker)getAppointmentsPast"
//            case .GetAppointmentsUpcoming:      return "\(APPURL.BaseURLSeeker)getAppointmentsUpcoming"
//            case .ReviewList:                   return "\(APPURL.BaseURLSeeker)reviews"
//            case .NotificationSetting:          return "\(APPURL.BaseURLStylist)notificationSetting"
//            case .StylistTimeSlots:             return "\(APPURL.BaseURLSeeker)stylistTimeSlots"
//            case .BookAppointment:              return "\(APPURL.BaseURLSeeker)bookAppointment"
//            case .GetTimeslots:                 return "\(APPURL.BaseURLSeeker)getTimeslots"
//            case .ApplyPromocode:               return "\(APPURL.BaseURLSeeker)applypromocode"
//            case .CalculateTravelCharge:        return "\(APPURL.BaseURLSeeker)calculatetravelcharge"
//            case .EditAppointment:              return "\(APPURL.BaseURLSeeker)editappointment"
//            case .CancelAppointment:            return "\(APPURL.BaseURLSeeker)cancelappointment"
//            case .Review:                       return "\(APPURL.BaseURLSeeker)review"
//            case .StylistTravelzone:            return "\(APPURL.BaseURLSeeker)stylistTravelzone"
//            case .StylistProfile:               return "\(APPURL.BaseURLSeeker)stylistprofile"
//
//            }
//        }
//    }
    
  
    // MARK: - GET/Json
    func getResponseFor(strUrl : String, parameters: NSDictionary? , completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        let request = NSMutableURLRequest(url: URL(string: strUrl)!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        

         let secretKey = SharedPreference.authToken()
//        if AppTheme.getIsUserLogin() {
//            let secretKey = AppTheme.getSecretKey()
            request.setValue(secretKey, forHTTPHeaderField: "authorization")
//        }
        
        if parameters != nil {
            var data : Data?
            do {
                data = try JSONSerialization.data(withJSONObject: parameters!, options: JSONSerialization.WritingOptions(rawValue: 0))
                request.httpBody = data
            } catch {
                data = Data()
            }
        }
        self.callWebservice(request as URLRequest?) { (responseData) in
            
            DispatchQueue.main.async(execute: {
                if responseData is NSDictionary {
                    // handle data here1
                    if  responseData["status"] != nil {
                        
                        let status = String(describing: responseData["status"] as AnyObject).lowercased()
                        
                        if status == "success" || status == "true" {
                            completion("success", responseData as AnyObject)
                        } else if status == "1001" {
                            print("Version Update")
                           // AppTheme.versionUpdated()
                        } else if status == "failed" || status == "false" || status == "0" {
                            
                            if let responceMsg : NSError = responseData["message"] as? NSError {
                                let data : NSMutableDictionary = responseData as! NSMutableDictionary
                                data["message"] = responceMsg.localizedDescription
                                
                                completion("error", data as AnyObject)
                            } else {
                                
                                if responseData["message"] as! String == "Invalid Token" {
                             //       AppTheme.tokenExpired()
                                } else {
                                    completion("error", responseData as AnyObject)
                                }
                            }
                            
                        } 
                    } else {
                        //call back else case
                        completion("error", "Netwrok Connection Lost" as AnyObject)
                    }
                }
            })
        }
    }

    
    // MARK: - Get/ Query param
    func getResponseForQueryParam(StrUrl : String, parameters: String, completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        var serviceStr = "\(StrUrl)\(parameters)"
        serviceStr = serviceStr.replacingOccurrences(of: " ", with: "%20")

        
        let request = NSMutableURLRequest(url: URL(string: serviceStr)!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let secretKey = SharedPreference.authToken()
        //        if AppTheme.getIsUserLogin() {
        //            let secretKey = AppTheme.getSecretKey()
        request.setValue(secretKey, forHTTPHeaderField: "authorization")
        //        }

        
        self.callWebservice(request as URLRequest?) { (responseData) in
            
            DispatchQueue.main.async(execute: {
                if responseData is NSDictionary {
                    // handle data here1
                    if  responseData["status"] != nil {
                        
                        let status = String(describing: responseData["status"] as AnyObject).lowercased()
                        
                        if status == "success" || status == "true" {
                            completion("success", responseData as AnyObject)
                        } else if status == "1001" {
                            print("Version Update")
                            //AppTheme.versionUpdated()
                        } else if status == "failed" || status == "false" || status == "0" {
                            
                            if let responceMsg : NSError = responseData["message"] as? NSError {
                                let data : NSMutableDictionary = responseData as! NSMutableDictionary
                                data["message"] = responceMsg.localizedDescription
                                
                                completion("error", data as AnyObject)
                            } else {
                                
                                if responseData["message"] as! String == "Invalid Token" {
                                   // AppTheme.tokenExpired()
                                } else {
                                    completion("error", responseData as AnyObject)
                                }
                            }
                        }
                    } else {
                        //call back else case
                        completion("error", "Netwrok Connection Lost" as AnyObject)
                    }
                }
            })
        }
    }
    
    
    
    // MARK: - Post/Json
    func getResponseForPost(strUrl : String, parameters: NSDictionary? , completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        let request = NSMutableURLRequest(url: URL(string: strUrl)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
       
//        if AppTheme.getIsUserLogin() {
//            let secretKey = AppTheme.getSecretKey()
            let secretKey = SharedPreference.authToken()
            request.setValue(secretKey, forHTTPHeaderField: "authorization")
//        }
        
        
        if parameters != nil {
            var data : Data?
            do {
                data = try JSONSerialization.data(withJSONObject: parameters!, options: JSONSerialization.WritingOptions(rawValue: 0))
                request.httpBody = data
            } catch {
                data = Data()
            }
        }
        self.callWebservice(request as URLRequest?) { (responseData) in
            
            DispatchQueue.main.async(execute: {
                if responseData is NSDictionary {
                    // handle data here1
                    if  responseData["status"] != nil {
                        
                        let status = String(describing: responseData["status"] as AnyObject).lowercased()
                        
                        if status == "success" || status == "true" {
                            completion("success", responseData as AnyObject)
                        } else if status == "1001" {
                            print("Version Update")
                            //AppTheme.versionUpdated()
                        } else if status == "failed" || status == "false" || status == "0" {
                            
                            if let responceMsg : NSError = responseData["message"] as? NSError {
                                let data : NSMutableDictionary = responseData as! NSMutableDictionary
                                data["message"] = responceMsg.localizedDescription
                                
                                completion("error", data as AnyObject)
                            } else {
                                
                                if responseData["message"] as! String == "Invalid Token" {
                                    //AppTheme.tokenExpired()
                                } else {
                                    completion("error", responseData as AnyObject)
                                }
                            }
                            
                        }
                    } else {
                        //call back else case
                        completion("error", "Netwrok Connection Lost" as AnyObject)
                    }
                }
            })
        }
    }
    
    // MARK: - Post/Param
    func getResponseForParamType(strUrl : String, parameters: NSDictionary?, completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        let body = NSMutableData()
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL)
        request.httpMethod = "POST";
        
        let secretKey = SharedPreference.authToken()
        request.setValue(secretKey, forHTTPHeaderField: "authorization")
        
        let boundary = generateBoundaryString()
        //define the multipart request type
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data( "--\(boundary)\r\n".utf8))
                body.append(Data( "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data( "\(value)\r\n".utf8))
            }
        }
        body.append(Data( "--\(boundary)--\r\n".utf8))
        request.httpBody = body as Data
        
        self.callWebservice(request as URLRequest?) { (responseData) in
            DispatchQueue.main.async(execute: {
                if responseData is NSDictionary {
                    print(responseData)
                    if  responseData["status"] != nil {
                        
                        let status = String(describing: responseData["status"] as AnyObject).lowercased()
                        
                        if status == "success" || status == "true" {
                            completion("success", responseData as AnyObject)
                        } else if status == "1001" {
                            print("Version Update")
                            //AppTheme.versionUpdated()
                        } else if status == "failed" || status == "false" || status == "0" {
                            
                            if let responceMsg : NSError = responseData["message"] as? NSError {
                                completion("error", responceMsg.localizedDescription as AnyObject)
                            } else {
                                
                                if responseData["message"] as! String == "Invalid Token" {
                                    //AppTheme.tokenExpired()
                                } else {
                                    completion("error", responseData["message"] as AnyObject)
                                }
                            }
                            
                        } else  {
                            completion("error", responseData["message"] as AnyObject)
                        }
                    }
                } else {
                    //call back else case
                    completion("error", "Netwrok Connection Lost" as AnyObject)
                }
            })
        }
    }
    

    
    // MARK: - Post/Param (Multipart- Image)
    //My function for sending data with image AAKASH VISHWAKARMA
    func getResponseForMultipartType(strUrl : String, parameters: NSDictionary?, imagesData : [Data], imageKey: [String] ,  completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        //filePath
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL)
        request.httpMethod = "POST";
        let secretKey = SharedPreference.authToken()
        request.setValue(secretKey, forHTTPHeaderField: "authorization")
        
        
        let boundary = generateBoundaryString()
        //define the multipart request type
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
//        let imageData = UIImageJPEGRepresentation(userimage, 1)
//        if(imageData==nil)  { return }
        
        request.httpBody = createBodyWithParameters(parameters: parameters, imageKey: imageKey , imageData: imagesData , boundary: boundary) as Data
        self.callWebservice(request as URLRequest?) { (responseData) in
            
            DispatchQueue.main.async(execute: {
                if responseData is NSDictionary {
                    // handle data here1
                    if  responseData["status"] != nil {
                        
                        let status = String(describing: responseData["status"] as AnyObject).lowercased()
                        
                        if status == "success" || status == "true" {
                            completion("success", responseData as AnyObject)
                        } else if status == "1001" {
                            print("Version Update")
                            //AppTheme.versionUpdated()
                        } else if status == "failed" || status == "false" {
                            if responseData["message"] as! String == "Invalid Token" {
                                //AppTheme.tokenExpired()
                            }
                            completion("error", responseData["message"] as AnyObject)
                        } else if status == "0" {
                            completion("error", responseData as AnyObject)
                        } else {
                            completion("error", responseData as AnyObject)
                        }
                    } else {
                        //call back else case
                        completion("error", "Netwrok Connection Lost" as AnyObject)
                    }
                } else {
                    //call back else case
                    completion("error", "Netwrok Connection Lost" as AnyObject)
                }
            })
            
        }
    }
    
    func createBodyWithParameters(parameters: NSDictionary?, imageKey: [String], imageData: [Data], boundary: String) -> NSData {
        
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        
        if !imageData.isEmpty {
            for i in 0...imageData.count-1 {
                if !imageData[i].isEmpty {
                    let filename = "user-profile\(i).jpg"
                    let mimetype = "image/jpg"
                    
                    body.append(Data( "--\(boundary)\r\n".utf8))
                    body.append(Data( "Content-Disposition: form-data; name=\"\(imageKey[i])\"; filename=\"\(filename)\"\r\n".utf8))
                    body.append(Data( "Content-Type: \(mimetype)\r\n\r\n".utf8))
                    body.append(imageData[i] as Data)
                    body.append(Data( "\r\n".utf8))
                    body.append(Data( "--\(boundary)--\r\n".utf8))
                }
            }
        }

        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    
    func downloadImage(fromURL: URL,completion:@escaping (_ responseData : AnyObject) -> ()) {
        let task = URLSession.shared.dataTask(with: fromURL, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                let errorDictionary = NSMutableDictionary()
              //  errorDictionary.setValue("0", forKey: WebServiceKey.Status.rawValue)
               // errorDictionary.setValue(error!.localizedDescription, forKey: WebServiceKey.Message.rawValue)
                completion(errorDictionary)
            } else {
                completion(data as AnyObject)
            }
        })
        task.resume()
    }
    
   
    func callWebservice(_ request: URLRequest!, completion:@escaping (_ responseData : NSDictionary) -> ()) {
        
        let task = URLSession.shared
            .dataTask(with: request, completionHandler: {
                (data, response, error) -> Void in
                if (error != nil) {
                    print(error!.localizedDescription)
                    let errorDictionary = NSMutableDictionary()
                   // errorDictionary.setValue("0", forKey: WebServiceKey.Status.rawValue)
                    //errorDictionary.setValue(error!.localizedDescription, forKey: WebServiceKey.Message.rawValue)
                    completion(errorDictionary)
                } else {
                    do {
                        //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                        let parsedJSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                        completion((parsedJSON as AnyObject) as! NSDictionary)
                    } catch let JSONError as NSError {
                        let errorDictionary = NSMutableDictionary()
                       // errorDictionary.setValue("0", forKey: WebServiceKey.Status.rawValue)
                       // errorDictionary.setValue(JSONError, forKey: WebServiceKey.Message.rawValue)
                        completion(errorDictionary)
                    }
                }
            })
        task.resume()
    }
    
    func showAlert(title:String , message : String , cancelButtonTitle:String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        
        return alert
    }
    


}




