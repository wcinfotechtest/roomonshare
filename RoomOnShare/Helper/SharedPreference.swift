
import UIKit

class SharedPreference: NSObject {
    
    fileprivate let kUserSuburb = "__u_suburb"
    fileprivate let kUserID = "__u_id"
    fileprivate let kUserAddress = "__u_address"
    fileprivate let kCarRepaire = "__u_car_repaire"
    fileprivate let kUserEmail = "__u_email"
    fileprivate let kUserFirstName = "__u_f_nm"
    fileprivate let kUserLastName = "__u_l_nm"
    fileprivate let kProfileImage = "__u_profile_img"
    fileprivate let kProfileImageUrl = "__u_profile_img_url"
    
    fileprivate let kToken = "__u_token"
    fileprivate let kStylemee_suburbs = "__u_stylemee_suburbs"
    fileprivate let kStylemee_suburbs_category = "__u_stylemee_suburbs_category"
    fileprivate let kStylemee_suburbs_id = "__u_stylemee_suburbs_id"
    fileprivate let kStylemee_suburbs_postcode = "__u_stylemee_suburbs_postcode"
    fileprivate let kStylemee_suburbs_status = "__u_stylemee_suburbs_status"
    fileprivate let kUsername = "__u_username"
    fileprivate let kEmail_verified = "__u_email_verified"
    
    fileprivate let kAuthKey = "__u_auth_key"
    fileprivate let kUserImage = "__u_user_img"

    fileprivate let kUserExperiance = "__u_exp_"
    fileprivate let kUserQualification = "__u_qua_"
    fileprivate let kUserType = "__u_type"
    fileprivate let kUserMobile = "__u_mobile"
    fileprivate let kDeviceToken = "__k_device_token_name";
    fileprivate let kUDID = "__k_device_UDID";
    fileprivate let kExpertise = "__k_expertise";
    fileprivate let kServiceType = "__k_service_type";
    fileprivate let kUserPhy = "__u_phy"
    fileprivate let kUserTip = "__u_tip"
    fileprivate let kUserPro = "__u_proy"
    fileprivate let KFilterData = "__filter_data"
    
    fileprivate let kStylemee_user_push_notify_status = "__u_stylemee_user_push_notify_status"
    fileprivate let kStylemee_user_text_notify_status = "__u_stylemee_user_text_notify_status"
    fileprivate let KStylemee_user_mail_notify_status = "__u_stylemee_user_mail_notify_status"
    
    
    fileprivate let defaults = UserDefaults.standard
    static let sharedInstance = SharedPreference()
    
//    class func saveUserData(_ user: UserEntity) {
//        sharedInstance.saveUserData(user)
//    }
//
//
//    fileprivate func saveUserData(_ user: UserEntity) {
//
//        defaults.setValue(user.stylemee_user_id  , forKey: kUserID)
//        defaults.setValue(user.stylemee_user_first_name, forKey: kUserFirstName)
//        defaults.setValue(user.stylemee_user_last_name, forKey: kUserLastName)
//        defaults.setValue(user.stylemee_user_address1, forKey: kUserAddress)
//        defaults.setValue(user.stylemee_user_email, forKey: kUserEmail)
//        defaults.setValue(user.stylemee_user_phone_no, forKey: kUserMobile)
//        defaults.setValue(user.stylemee_profile_experience, forKey: kUserExperiance)
//        defaults.setValue(user.stylemee_profile_training, forKey: kUserQualification)
//        defaults.setValue(user.stylemee_profile_beauty_philosophy, forKey: kUserPhy)
//        defaults.setValue(user.stylemee_profile_beauty_tips, forKey: kUserTip)
//        defaults.setValue(user.stylemee_profile_favt_product, forKey: kUserPro)
//        defaults.setValue(user.stylemee_user_othersuburbs, forKey: kUserSuburb)
//        defaults.setValue(user.stylemee_user_img, forKey: kProfileImage)
//        defaults.setValue(user.stylemee_user_img_url, forKey: kProfileImageUrl)
//
//        defaults.setValue(user.token , forKey: kToken)
//        defaults.setValue(user.stylemee_suburbs, forKey: kStylemee_suburbs)
//        defaults.setValue(user.stylemee_suburbs_category, forKey: kStylemee_suburbs_category)
//        defaults.setValue(user.stylemee_suburbs_id, forKey: kStylemee_suburbs_id)
//        defaults.setValue(user.stylemee_suburbs_postcode, forKey: kStylemee_suburbs_postcode)
//        defaults.setValue(user.stylemee_suburbs_status, forKey: kStylemee_suburbs_status)
//        defaults.setValue(user.username, forKey: kUsername)
//        defaults.setValue(user.email_verified, forKey: kEmail_verified)
//
//        defaults.setValue(user.stylemee_user_mail_notify_status, forKey: KStylemee_user_mail_notify_status )
//        defaults.setValue(user.stylemee_user_push_notify_status, forKey: kStylemee_user_push_notify_status )
//        defaults.setValue(user.stylemee_user_text_notify_status, forKey: kStylemee_user_text_notify_status )
//
//        UIImageView().kf.setImage(with: NSURL(string: user.stylemee_user_img_url.getURLstring)! as URL, placeholder: #imageLiteral(resourceName: "placeholder") , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
//            if image != nil {
//                SharedPreference.storeUserImage(UIImageJPEGRepresentation(image!, 0.5)!)
//            }
//        })
//
//        defaults.synchronize()
//    }
    
    
    
    
    
//    class func clearUserData(){
//        sharedInstance.clearUserData()
//        sharedInstance.setUserImage(Data())
//    }
    
//    fileprivate func clearUserData(){
//        self.saveUserData(UserEntity())
//
//    }
//
//    class func getUserData() -> UserEntity{
//        return sharedInstance.getUserData()
//    }
//
//    fileprivate  func getUserData() -> UserEntity {
//
//        let user:UserEntity     =  UserEntity()
//        user.stylemee_user_id                  =  defaults.value(forKey: kUserID)  as? Int ?? 0
//        user.stylemee_user_first_name          =  defaults.value(forKey: kUserFirstName) as? String  ?? ""
//        user.stylemee_user_last_name          =  defaults.value(forKey: kUserLastName) as? String  ?? ""
//        user.stylemee_user_img                 =  defaults.value(forKey: kProfileImage) as? String ?? ""
//        user.stylemee_user_img_url              =  defaults.value(forKey: kProfileImageUrl) as? String ?? ""
//        user.stylemee_user_phone_no            = defaults.value(forKey: kUserMobile) as? String ?? ""
//        user.stylemee_profile_experience                 =  defaults.value(forKey: kUserExperiance) as? String ?? ""
//        user.stylemee_profile_training            = defaults.value(forKey: kUserQualification) as? String ?? ""
//        user.stylemee_profile_favt_product            = defaults.value(forKey: kUserPro) as? String ?? ""
//        user.stylemee_profile_beauty_tips            = defaults.value(forKey: kUserTip) as? String ?? ""
//        user.stylemee_profile_beauty_philosophy            = defaults.value(forKey: kUserPhy) as? String ?? ""
//
//        user.stylemee_user_email               = defaults.value(forKey: kUserEmail) as? String ?? ""
//        user.stylemee_user_address1            = defaults.value(forKey: kUserAddress) as? String ?? ""
//        user.stylemee_user_othersuburbs            = defaults.value(forKey: kUserSuburb) as? String ?? ""
//
//        user.token      = defaults.value(forKey: kToken) as? String ?? ""
//        user.stylemee_suburbs       = defaults.value(forKey: kStylemee_suburbs) as? String ?? ""
//        user.stylemee_suburbs_category      = defaults.value(forKey: kStylemee_suburbs_category) as? String ?? ""
//        user.stylemee_suburbs_id        = defaults.value(forKey: kStylemee_suburbs_id) as? Int ?? 0
//        user.stylemee_suburbs_postcode      = defaults.value(forKey: kStylemee_suburbs_postcode) as? String ?? ""
//        user.stylemee_suburbs_status        = defaults.value(forKey: kStylemee_suburbs_status) as? Int ?? 0
//        user.username       = defaults.value(forKey: kUsername) as? String ?? ""
//        user.email_verified      = defaults.value(forKey: kEmail_verified) as? Int ?? 0
//
//        user.stylemee_user_mail_notify_status      = defaults.value(forKey: KStylemee_user_mail_notify_status) as? Int ?? 0
//        user.stylemee_user_push_notify_status      = defaults.value(forKey: kStylemee_user_push_notify_status) as? Int ?? 0
//        user.stylemee_user_text_notify_status      = defaults.value(forKey: kStylemee_user_text_notify_status) as? Int ?? 0
//
//        return user
//    }
    
    
   // device token
    class func storeDeviceToken(_ token: String) {
        sharedInstance.setDeviceToken(token)
    }
    class func deviceToken() -> String {
        return sharedInstance.getDeviceToken() ?? ""
    }
    fileprivate func setDeviceToken(_ token: String){
        defaults.set(token, forKey: kDeviceToken);
    }
    fileprivate func getDeviceToken() -> String?{
        return defaults.value(forKey: kDeviceToken) as? String;
    }

    // AuthToken
    class func storeAuthToken(_ token: String) {
        sharedInstance.setAuthToken(token)
    }
    class func authToken() -> String {
        return sharedInstance.getAuthToken() ?? ""
    }
    fileprivate func setAuthToken(_ token: String){
        defaults.set(token, forKey: kAuthKey);
    }
    fileprivate func getAuthToken() -> String?{
        return defaults.value(forKey: kAuthKey) as? String;
    }
    
    
    // UDID
    class func storeUDID(_ token: String) {
        sharedInstance.setUDID(token)
    }
    class func UDID() -> String {
        return sharedInstance.getUDID() ?? "123456789"
    }
    fileprivate func setUDID(_ token: String){
        defaults.set(token, forKey: kUDID);
    }
    fileprivate func getUDID() -> String?{
        return defaults.value(forKey: kUDID) as? String;
    }
    
     // user Image
    class func storeUserImage(_ img: Data) {
        sharedInstance.setUserImage(img)
    }
    class func userImage() -> Data {
        return sharedInstance.getUserImage() ?? Data()
    }
    fileprivate func setUserImage(_ img: Data){
        defaults.set(img, forKey: kUserImage)
    }
    fileprivate func getUserImage() -> Data?{
        return defaults.value(forKey: kUserImage) as? Data
    }
    
    
    

    // Filter data
//    class func storeFilterData (_ data: FilterData) {
//
//        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: [data])
//        sharedInstance.setFilterData(encodedData)
//    }
//    class func filterData() -> FilterData {
//        let filterData = NSKeyedUnarchiver.unarchiveObject(with: sharedInstance.getFilterData()!) as! FilterData
//        return filterData
//    }
//    fileprivate func setFilterData(_ data: Data){
//        defaults.set(data, forKey: KFilterData);
//    }
//    fileprivate func getFilterData() -> Data?{
//        return defaults.value(forKey: KFilterData) as? Data
//    }
    
    
}
