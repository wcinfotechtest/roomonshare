//
//  Add.swift
//  RoomOnShare
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class Add: UIView {

    @IBOutlet weak var imgCover: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
    
    }
    
    

}
