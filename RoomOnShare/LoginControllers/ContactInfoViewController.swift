//
//  ContactInfoViewController.swift
//  RoomOnShare
//
//  Created by mac on 01/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ContactInfoViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtWhoContact: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumbe: UITextField!
    @IBOutlet weak var txtAdvice: UITextField!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    var Imagepicker = UIImagePickerController()
    //let
    
    var arrOption = ["one", "two", "three", "seven", "fifteen"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Imagepicker.delegate = self
        self.txtWhoContact.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "BedroomDescriptionViewController")
    }
    
    @IBAction func CkeckBoxMethod(_ sender: Any) {
        guard let FirstN = self.txtFirstName.text, FirstN != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter first name", VC: self)
        }
        
        guard let LastN = self.txtLastName.text, LastN != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter last name", VC: self)
        }
        
        guard let Who = self.txtWhoContact.text, Who != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please select one option", VC: self)
        }

        guard let CountryCode = self.txtCountryCode.text, CountryCode != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please select country code", VC: self)
        }
        
        guard let Mobile = self.txtMobileNumbe.text, Mobile != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter mobile number", VC: self)
        }
        
        guard Common.isValidateNumber(value: self.txtMobileNumbe.text!) else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter valid mobile number", VC: self)
        }
        
        //guard let Advice = self.txtMobileNumbe.text, Advice != ""  else {
          //  return Common.ShowAlert(Title: "Alert", Message: "Please enter mobile number", VC: self)
        //}
        
        
    }
    
    @IBAction func EditImageMethod(_ sender: Any) {
        Common.ActionSheetForGallaryAndCamera(Picker: Imagepicker, VC: self)
    }
    
   

}

extension ContactInfoViewController : UITextFieldDelegate, MyPickerViewProtocol {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtWhoContact {
            let myPickerView =  MyPickerView.init(withDictionary: arrOption)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtWhoContact.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(selectedRowValue: Int?) {
        let item = arrOption[selectedRowValue!]
        self.txtWhoContact.text = item
        print(item)
    }
}

extension ContactInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           self.txtWhoContact.text = arrOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrOption[row]
    }
    
}


extension ContactInfoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


