//
//  SignUpViewController.swift
//  RoomOnShare
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtCountry: UITextField!    
    @IBOutlet var Gender: [UIButton]!
    var checkGender : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.backgroundColor = UIColor.red
            }else{
                button.backgroundColor = UIColor.blue
            }
        }
    }
    
    //////// Gender /////////
    @IBAction func GenderMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.Gender, Selected: Selectedtag)
    }
    
    //////// Signup /////////
    @IBAction func SignUpMethod(_ sender: Any) {
        
        Common.PushMethod(VC: self, identifier: "VerifyViewController")
        
//        guard let Fname = self.txtEmail.text, Fname != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter fullname", VC: self)
//        }
//
//        guard let email = self.txtEmail.text, email != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter E-mail", VC: self)
//        }
//
//        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter valid E-mail", VC: self)
//        }
//
//        guard  self.checkGender != 0 else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please select gender", VC: self)
//        }
//
//        guard let CountryCode = self.txtCountryCode.text, CountryCode != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please select country code", VC: self)
//        }
//
//        guard let Mobile = self.txtMobileNumber.text, Mobile != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter mobile number", VC: self)
//        }
//
//        guard Common.isValidateNumber(value: self.txtMobileNumber.text!) else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter valid mobile number", VC: self)
//        }
//
//        guard let Country = self.txtCountry.text, Country != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter country name", VC: self)
//        }
//
//        guard let password = self.txtPassword.text, password != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter password", VC: self)
//        }
//
//        guard Common.isCheckPasswordLength(password: self.txtPassword.text!)  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Your password is too short", VC: self)
//        }

    }
    
    //////// Login /////////
    @IBAction func Login(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "LoginViewController")
    }
    
    
    @IBAction func GoogleMethod(_ sender: Any) {
        
    }
    
    @IBAction func LinkedinMethod(_ sender: Any) {
        
    }
    
}
