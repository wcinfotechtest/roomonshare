//
//  VerifyViewController.swift
//  RoomOnShare
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class VerifyViewController: UIViewController {

    @IBOutlet weak var txtOTP: UITextField!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func VerifyMethod(_ sender: Any) {
        if Common.isCheckPasswordLength(password: txtOTP.text!) {
            
        }else{
            return Common.ShowAlert(Title: "Alert", Message: "Please enter OTP", VC: self)
        }
    }
    
    @IBAction func ResendMethod(_ sender: Any) {
        
    }
    
}
