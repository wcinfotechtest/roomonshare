//
//  LoginViewController.swift
//  RoomOnShare
//
//  Created by mac on 27/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift
import Crashlytics

class LoginViewController: UIViewController {

    @IBOutlet weak var btnFacebook: UIButton!    
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnlogin: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        //GIDSignIn.sharedInstance().delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///////////////////////// Facebook Signin ///////////////////////
    @IBAction func FacebookMethod(_ sender: UIButton) {
        
        SocialClass.Facebook(self.btnlogin as! FBSDKLoginButton, Vc: self) { (result) in
        print(result)
            
//        {
//            email = "wcinfotechtest@gmail.com";
//            "first_name" = Westcoast;
//            id = 131531871106201;
//            "last_name" = Infotech;
//            name = "Westcoast Infotech";
//            picture =     {
//                data =         {
//                    height = 200;
//                    "is_silhouette" = 1;
//                    url = "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=131531871106201&height=200&width=200&ext=1535889893&hash=AeQCAe1zEltuLyzo";
//                    width = 200;
//                };
//            };
//            }

        }
    }

    ///////////////////////// Google Signin ///////////////////////
    @IBAction func GoogleMethod(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }

    ///////////////////////// LinkedIn Signin ///////////////////////
    @IBAction func Linkin(_ sender: UIButton) {
        SocialClass.LinkedIn(vc: self) { (result) in
            print(result)
        }
    }
    
    @IBAction func ForgotPasswordMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ForgotPasswordViewController")
    }
    
    @IBAction func LoginMethod(_ sender: Any) {
//        guard let email = self.txtEmail.text, email != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter E-mail", VC: self)
//        }
//
//        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter valid E-mail", VC: self)
//        }
//
//        guard let password = self.txtPassword.text, password != ""  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter password", VC: self)
//        }
//
//        guard Common.isCheckPasswordLength(password: self.txtPassword.text!)  else {
//            return Common.ShowAlert(Title: "Alert", Message: "Your password is too short", VC: self)
//        }

        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MatchesViewController") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)        
    }
    
    @IBAction func NotRegistredMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "SignUpViewController")
    }
    
}


extension LoginViewController :  GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            print("\(userId!), \(idToken!), \(fullName!), \(givenName!), \(familyName!), \(email!)")
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}
