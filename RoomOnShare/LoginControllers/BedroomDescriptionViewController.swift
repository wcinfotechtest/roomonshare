//
//  BedroomDescriptionViewController.swift
//  RoomOnShare
//
//  Created by mac on 07/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class BedroomDescriptionViewController: UIViewController {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var mainviewHight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view1.isHidden = true
        self.view2.isHidden = true
        self.view3.isHidden = true
        mainviewHight.constant = mainviewHight.constant - 200
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        
    }
    
}
