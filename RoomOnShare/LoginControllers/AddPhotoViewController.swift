//
//  AddPhotoViewController.swift
//  RoomOnShare
//
//  Created by mac on 02/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class AddPhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    
    var picker = UIImagePickerController()
    var state : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add Person Photos
        self.picker.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Btn1Method(_ sender: Any) {
        self.state = 1
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func btn2Method(_ sender: Any) {
        self.state = 2
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func Btn3Method(_ sender: Any) {
        self.state = 3
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func Btn4Method(_ sender: Any) {
        self.state = 4
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func Btn5Method(_ sender: Any) {
        self.state = 5
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PreferredLoactionViewController") as! PreferredLoactionViewController
     Vc.check = "AddPhoto"
        self.navigationController?.pushViewController(Vc, animated: false)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage]
        
        switch self.state {
        case 1:
            self.img1.image = image as! UIImage?
        case 2:
            self.img2.image = image as! UIImage?
        case 3:
            self.img3.image = image as! UIImage?
        case 4:
            self.img4.image = image as! UIImage?
        case 5:
            self.img5.image = image as! UIImage?
        default:
            self.img1.image =  UIImage(named: "")
        }
        
         dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
