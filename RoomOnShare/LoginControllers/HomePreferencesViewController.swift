//
//  HomePreferencesViewController.swift
//  RoomOnShare
//
//  Created by mac on 08/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class HomePreferencesViewController: UIViewController {

    @IBOutlet weak var sliderRent: UISlider!
    @IBOutlet weak var lblShowRent: UILabel!
    @IBOutlet weak var txtBedroomType: UITextField!
    @IBOutlet var HomeSize: [UIButton]!
    @IBOutlet var BedroomSize: [UIButton]!
    @IBOutlet weak var txtBathroomFacility: UITextField!
    @IBOutlet weak var txtParkingFacility: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
    }
    
    @IBAction func HomeSizeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.HomeSize, Selected: Selectedtag)
    }
    
    @IBAction func BedroomSizeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.BedroomSize, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.backgroundColor = UIColor.red
            }else{
                button.backgroundColor = UIColor.blue
            }
        }
    }
    
    
    @IBAction func NextMethod(_ sender: Any) {
    }
    
}
