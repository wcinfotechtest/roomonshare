//
//  StartViewController.swift
//  RoomOnShare
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift

class StartViewController: UIViewController {
    
    @IBOutlet weak var btnlogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //////// Signup /////////
    @IBAction func EmailMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "SignUpViewController")
    }
    
    //////// Facebook /////////
    @IBAction func FacebookMethod(_ sender: Any) {
        SocialClass.Facebook(self.btnlogin as! FBSDKLoginButton, Vc: self) { (result) in
            print(result)
        }
            
            //        {
            //            email = "wcinfotechtest@gmail.com";
            //            "first_name" = Westcoast;
            //            id = 131531871106201;
            //            "last_name" = Infotech;
            //            name = "Westcoast Infotech";
            //            picture =     {
            //                data =         {
            //                    height = 200;
            //                    "is_silhouette" = 1;
            //                    url = "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=131531871106201&height=200&width=200&ext=1535889893&hash=AeQCAe1zEltuLyzo";
            //                    width = 200;
            //                };
            //            };
            //            }
    }
    
    //////// Google /////////
    @IBAction func GoogleMethod(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    //////// LinkedIn /////////
    @IBAction func LinkedInMethod(_ sender: Any) {
        SocialClass.LinkedIn(vc: self) { (result) in
            print(result)
        }
    }
    
    //////// Login /////////
    @IBAction func LoginMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "LoginViewController")
    }
    
}

extension StartViewController :  GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            print("\(userId!), \(idToken!), \(fullName!), \(givenName!), \(familyName!), \(email!)")
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

//print(Userdata)
//        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) {
//            (result, error) in
//            if (error == nil) {
//                let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                if fbloginresult.grantedPermissions != nil {
//                    if (fbloginresult.grantedPermissions.contains("email")){
//                        self.getFBUserData()
//                        let myFbToken = (FBSDKAccessToken.current().tokenString) as! String
//                        print(myFbToken)
//                        print(FBSDKAccessToken.current().tokenString)
//                        fbLoginManager.logOut()
//                    }
//                }
//            }
//        }

//    func getFBUserData() {
//        if ((FBSDKAccessToken.current()) != nil) {
//            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result,error) -> Void in
//                if (error == nil){
//                    print(result!)
//                    let dict = result as! NSDictionary
//                    let userDict = dict.value(forKey:"id") as! String
//                    print(userDict)
//                }
//            })
//        }
//    }


//   let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81zhiuczjmy4v9", clientSecret: "5nKx1TeI0JEYjsmF", state: "linkedin\(Int(Date().timeIntervalSince1970))", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.linkedin.com/oauth/v2/accessToken"))

//        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
//            print("token----\(lsToken)")
//            // let strToken = lsToken.accessToken
//            }, error: { [unowned self] (error) -> Void in
//                //self.writeConsoleLine("Encounter error: \(error.localizedDescription)")
//            }, cancel: { [unowned self] () -> Void in
//                // self.writeConsoleLine("User Cancelled!")
//        })
//
//        linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
//            print("Request success with response: \(response)")
//            DispatchQueue.main.async {
//                //  let login = self.storyboard?.instantiateViewController(withIdentifier: "HOMEViewController") as! HOMEViewController
//                //self.present(login, animated: true, completion: nil)
//            }
//        }) { [unowned self] (error) -> Void in
//        }

