//
//  AboutOccupantsViewController.swift
//  RoomOnShare
//
//  Created by mac on 07/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class AboutOccupantsViewController: UIViewController {
    
    
    @IBOutlet var Gender: [UIButton]!
    @IBOutlet var AgeGroup: [UIButton]!
    @IBOutlet var Smoking: [UIButton]!
    @IBOutlet var Pets: [UIButton]!
    @IBOutlet weak var txtInterest: UITextField!
    @IBOutlet weak var txtPersonalIntro: UITextField!
    @IBOutlet weak var txtPersonalQuality: UITextField!
    @IBOutlet weak var `switch`: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func GenderMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.Gender, Selected: Selectedtag)
    }
    
    @IBAction func AgeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.AgeGroup, Selected: Selectedtag)
    }
    
    @IBAction func SmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.Smoking, Selected: Selectedtag)
    }
    
    @IBAction func PetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.CheckUnceck(arrButton: self.Pets, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.backgroundColor = UIColor.red
            }else{
                button.backgroundColor = UIColor.blue
            }
        }
    }

    @IBAction func NextMethod(_ sender: Any) {
    }
    
}
