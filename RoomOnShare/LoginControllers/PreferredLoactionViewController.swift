//
//  PreferredLoactionViewController.swift
//  RoomOnShare
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation


class PreferredLoactionViewController: UIViewController {

    @IBOutlet weak var imgLocationLogo: UIImageView!
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var lblAddress: UILabel!
    
    var locationManager:CLLocationManager!
    var check : String = ""
    
    var latitude : Double = 0000
    var longitude : Double = 0000
     var placePickerController = GMSAutocompleteViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placePickerController.delegate = self
        
        determineMyCurrentLocation()
        
        if self.check == "AddPhoto" {
         self.viewBG.isHidden = true
        }else{
         self.viewBG.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func SelectLocationMethod(_ sender: Any) {
       present(placePickerController, animated: true, completion: nil)
    }
    
    @IBAction func EditLocationMethod(_ sender: Any) {
        present(placePickerController, animated: true, completion: nil)
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        
    }
    
}

extension PreferredLoactionViewController : CLLocationManagerDelegate{
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        self.latitude = userLocation.coordinate.latitude
        self.longitude = userLocation.coordinate.longitude
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}

extension PreferredLoactionViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.coordinate.latitude)")
        print("Place name: \(place.coordinate.longitude)")
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
       // print("Place attributions: \(place.attributions)")
        
        self.lblAddress.text = place.formattedAddress
        ShowMarker(lati: place.coordinate.latitude, long: place.coordinate.longitude)
        
        self.viewBG.isHidden = false
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func ShowMarker(lati : Double, long : Double) {
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 6.0)
        //let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        //view = mapView
        self.viewMap.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
        marker.map = self.viewMap
    }
    
}
