//
//  MatchPropertiesViewController.swift
//  RoomOnShare
//
//  Created by mac on 09/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class MatchPropertiesViewController: UIViewController {

    @IBOutlet weak var tblMatchProperties: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
}
    
extension MatchPropertiesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblMatchProperties.dequeueReusableCell(withIdentifier: "Matches") as! MatchesTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
}

    

