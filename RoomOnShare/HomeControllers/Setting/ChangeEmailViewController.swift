//
//  ChangeEmailViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ChangeEmailViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func SubmitMethod(_ sender: Any) {
        guard let email = self.txtEmail.text, email != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter E-mail", VC: self)
        }
        
        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter valid E-mail", VC: self)
        }
    }
    
}
