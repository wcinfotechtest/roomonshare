//
//  ResetPasswordViewController.swift
//  RoomOnShare
//
//  Created by mac on 01/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func ResetPasswordMethod(_ sender: Any) {
        guard let email = self.txtOTP.text, email != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter OTP", VC: self)
        }
        
        guard let pass = self.txtPassword.text, pass != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter password", VC: self)
        }
        
        guard let conpass = self.txtConfirmPassword.text, conpass != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter confirmpassword", VC: self)
        }
        
        guard  self.txtPassword.text == self.txtConfirmPassword.text   else {
            return Common.ShowAlert(Title: "Alert", Message: "Password not matched", VC: self)
        }
       
    }
    
}
