//
//  SettingViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    
    @IBOutlet weak var lblEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func ChangeEmailMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ChangeEmailViewController")
    }
    
    @IBAction func ChangePasswordMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ChangePasswordViewController")
    }
    
}
