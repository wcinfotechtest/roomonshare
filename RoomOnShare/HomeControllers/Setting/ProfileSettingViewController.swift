//
//  ProfileSettingViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ProfileSettingViewController: UIViewController {
    
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMsgNotification: UILabel!
    @IBOutlet weak var lblMatchesNotification: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func DeleteMethod(_ sender: Any) {
        
    }
    
    @IBAction func ActiveMethod(_ sender: Any) {
        
    }
    
    @IBAction func EditMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ChangeEmailViewController")
    }
    
    @IBAction func MsgNotificationMethod(_ sender: Any) {
    }
    
    @IBAction func MatchesNotificationMethod(_ sender: Any) {
    }
    
    @IBAction func SubmitMethod(_ sender: Any) {
    }
    
}
